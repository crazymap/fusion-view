# 自定义水球图

## 一、配置

### 1、图层

选中该水球图组件，在操作界面右侧的“图层名称”处修改该组件图层的名称。建议设置，该内容和左侧控制面板该图表插件名称一致，便于后期图表多时管理；
<viewer>
  <img src="../image/customWaterball-1.png" width="80%">
</viewer>

### 2、标题

选中该水球图组件，在操作界面右侧的“标题”处可修改该组件的标题。
<viewer>
  <img src="../image/customWaterball-2.png" width="80%">
</viewer>

- 标题：标题显示的内容，如果不想显示标题，不填写内容就不显示；

- 副标题：副标题内容，如果不想显示副标题，不填写内容就不显示；

- 标题位置：单选，分为「居左」「居中」「居右」，如下图；

- 标题字号：标题文字的字体大小；

- 标题粗细：标题文字字体的粗细，可选：
  > * 'normal'：正常
  >
  > * 'bold'：粗体
  >
  > * 'bolder'：加粗体
  >
  > * 'lighter'：较轻

- 字体样式：标题文字的字体系列；

### 3、轮廓

选中该水球图组件，在操作界面右侧的“轮廓”处可修改该属性设置。
<viewer>
  <img src="../image/customWaterball-3.png" width="80%">
</viewer>

- 展示轮廓开关：是否显示水球外层轮廓；

- 轮廓颜色：水球图轮廓的颜色；

- 轮廓间距：水球到轮廓之间的距离；

- 间距颜色：水球到轮廓之间的颜色；

- 轮廓粗细：水球轮廓的宽度；

- 阴影范围/颜色：轮廓外侧阴影的区域的范围/颜色；

<viewer>
  <img src="../image/customWaterball-3-1.png" width="40%">
  <img src="../image/customWaterball-3-1.png" width="40%">
</viewer>

### 4、水球

选中该水球图组件，在操作界面右侧的“水球”处可修改该属性设置。
<viewer>
  <img src="../image/customWaterball-4.png" width="80%">
</viewer>

- 水球形状：单选，选择水球图的外形样式，如：圆形、正方形、气泡等；
  <viewer>
    <img src="../image/customWaterball-4-1.png" width="30%">
    <img src="../image/customWaterball-4-2.png" width="30%">
    <img src="../image/customWaterball-4-3.png" width="30%">
  </viewer>

- 边框颜色/粗细：水球边框的颜色/粗细，如下图；
  <viewer>
    <img src="../image/customWaterball-4-5.png" width="40%">
    <img src="../image/customWaterball-4-5-1.png" width="40%">
  </viewer>

- 阴影范围/颜色：水球的阴影区域范围/颜色，如下图；
  <viewer>
    <img src="../image/customWaterball-4-6.png" width="40%">
    <img src="../image/customWaterball-4-6-1.png" width="40%">
  </viewer>

### 5、水纹

选中该水球图组件，在操作界面右侧的“水纹”处可修改该属性设置。
<viewer>
  <img src="../image/customWaterball-5.png" width="80%">
</viewer>

- 显示滚动开关：是否显示x轴；

- 水波数量：水球内部水波纹的数量；

- 滚动方向：水球内波纹滚动方向，示意图如下；
  <viewer>
    <img src="../image/customWaterball-5-1.png" width="60%">
  </viewer>

- 触发焦点透明度：鼠标聚焦时水纹透明度；

### 6、数值

选中该水球图组件，在操作界面右侧的“数值”处可修改该属性设置。
<viewer>
  <img src="../image/customWaterball-6.png" width="80%">
</viewer>

- 数值字号：水球内百分比字体大小；

- 数值样式：数值的字体样式；

- 数值粗细：数值的粗细，可选：
  > * 'normal'：正常
  >
  > * 'bold'：粗体
  >
  > * 'bolder'：加粗体
  >
  > * 'lighter'：较轻；

- 数值颜色：数值的颜色设置，如下图；

- 水内数值颜色：水内数值的颜色设置，如下图；
  <viewer>
    <img src="../image/customWaterball-6-1.png" width="40%">
    <img src="../image/customWaterball-6-1-1.png" width="40%">
  </viewer>

### 7、动画

选中该组件，在操作界面右侧的“动画”处可修改组件的载入动画效果。分别为：

- 弹跳

- 渐隐渐显

- 向右滑入

- 向左滑入

- 放缩

- 旋转

- 滚动

## 二、数据

静态数据格式：

```
  [0.6, 0.55, 0.4, 0.25]
```
