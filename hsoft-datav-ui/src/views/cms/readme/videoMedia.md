# 视频

## 一、配置

- 图层名称：组件的名称。（建议设置，方便后期组件管理）

- 视频地址：可设置网络视频地址，也可为静态资源相对路径，例如设置视频地址为
  "https://v-cdn.zjol.com.cn/280443.mp4"，如下图；

  <viewer>
  <img src="../image/video-1.png" width="60%">
  </viewer>

- 循环播放：设置视频是否结束后自动循环播放；

- 开启控制开关：设置在视频播放页面下方是否显示控制栏，设置开启，如下图；

  <viewer>
  <img src="../image/video-2.png" width="60%">
  </viewer>

- 视频库：服务器视频资源库，点击后打开视频列表页，如下图；

  <viewer>
  <img src="../image/video-3.png" width="60%">
  </viewer>

  - 可通过视频名称、所属模板查询视频列表;

  - 鼠标选中视频点击播放按钮可预览视频，点击视频名称选中后即可在页面创建该视频;

  - 点击视频右上角删除按钮即可在资源库中删除该视频;

  - 点击上传按钮即可打开上传视频页面，上传本地视频至资源库中;

  <viewer>
  <img src="../image/video-4.png" width="60%">
  </viewer>

  - 是否公开：选择公开全部用户都可在资源库中看见该视频，选择非公开仅自己能看见；

  - 所属模板：可自定义设置视频所属模板；

  - 上传按钮：点击该按钮可打开本地文件夹选择视频上传，视频大小不超过 200M；

- 载入动画：组件的载入动画效果。分别为：弹跳、渐隐渐显、向右滑入、向左滑入、放缩、旋转、滚动。
