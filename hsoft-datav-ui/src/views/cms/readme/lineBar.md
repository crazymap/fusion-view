# 折线图-柱状图

## 一、配置

### 1、图层

选中该折线图-柱状图组件，在操作界面右侧的“图层名称”处可修改组件的名称。（建议设置，方便后期组件管理）

### 2、标题

- 标题：标题显示的内容，例如设置标题为"主标题"，如下图；

- 副标题：副标题内容，例如设置副标题为"副标题"，如下图（如果不想显示副标题，不填写内容就不显示）；

  <viewer>
  <img src="../image/linebar-1.png" width="60%">
  </viewer>

### 3、图例

- 是否显示图例开关：该开关控制图例的显示与隐藏，显示图例情况如下图；

- 是否显示工具箱开关：该开关控制工具箱的显示与隐藏，显示工具箱情况如下图；

  <viewer>
  <img src="../image/linebar-1.png" width="60%">
  </viewer>

- 标记宽度：图例标记图标的宽度；

- 标记高度：图例标记图标的高度；

- 字号：图例字体大小;

- 字体颜色：图例字体颜色；

- 布局朝向：可选水平或垂直；

- 水平位置：图例距离图表的左边距；

- 垂直位置：图例距离图表的上边距；

### 4、x轴

- 是否显示 X 轴开关：X 轴是否显示，包括 X 轴线以及 X 轴标签；

- 是否显示 X 轴线开关：X 轴线是否显示，不影响X轴标签显示；

- X 轴颜色：X 轴线颜色；

- X 轴粗细：X 轴线宽度；

- X 轴字号：X 轴标签字体大小；

- X 轴字体颜色：X 轴标签字体颜色

### 4、y轴

- 左侧 Y 轴是否显示开关：控制图表左侧 Y 轴是否显示，包括轴线和标签；

- 右侧 Y 轴是否显示开关：控制图表右侧 Y 轴是否显示，设置右侧 Y 轴不显示如下图；

  <viewer>
  <img src="../image/linebar-3.png" width="60%">
  </viewer>

- 是否显示左侧 Y 轴线开关：左侧Y 轴线是否显示，不影响 Y 轴标签显示；

- 是否显示右侧 Y 轴线开关：右侧Y 轴线是否显示，不影响 Y 轴标签显示；

- Y 轴颜色：Y 轴线颜色；

- Y 轴粗细：Y 轴线宽度；

- XY 轴字号：X 轴标签字体大小；

- X 轴字体颜色：X 轴标签字体颜色

### 4、折线图

- 坐标轴名称：图表右侧 Y 轴名称，例如设置为“温度”，如下图；

- 刻度单位：图表右侧 Y 轴数值单位，例如设置为“°C”，如下图；

- 曲线是否平滑开关：控制折线图折点处曲线是否平滑，设置曲线平滑如下图；

- 是否显示分隔线开关：以右侧坐标轴刻度为分隔线设置是否显示，设置分隔线显示如下图；

- X 轴是否显示开关：控制 X 轴是否显示，设置 X 轴显示如下图；

  <viewer>
  <img src="../image/linebar-2.png" width="60%">
  </viewer>



### 5、柱状图

- 坐标轴名称：图表左侧 Y 轴名称，例如设置为“水量”，如下图；

- 刻度单位：图表左侧 Y 轴数值单位，例如设置为“ml”，如下图；

- 柱体宽度：柱体的宽度，可以调节改变，例如设置为 20，如下图；

- 是否显示分隔线开关：以左侧坐标轴刻度为分隔线设置是否显示，设置分隔线显示如下图；

  <viewer>
  <img src="../image/linebar-4.png" width="60%">
  </viewer>

- 左侧 Y 轴是否显示开关：控制图表左侧 Y 轴是否显示，设置左侧 Y 轴不显示如下图；

  <viewer>
  <img src="../image/linebar-5.png" width="60%">
  </viewer>

### 6、坐标轴

- X 轴颜色：设置 X 坐标轴以及 X 轴提示框背景颜色，例如设置 X 轴为红色，如下图；

- 左侧 Y 轴颜色：设置左侧 Y 轴以及左侧 Y 轴提示框背景颜色，例如设置为绿色，如下图；

- 右侧 Y 轴颜色：设置右侧 Y 轴以及右侧 Y 轴提示框背景颜色，例如设置为绿色，如下图；

  <viewer>
  <img src="../image/linebar-6.png" width="60%">
  </viewer>

### 7、辅助功能

- 显示平均线开关：控制每组折线图以及柱状图是否显示平均线，设置显示平均线，如下图；

  <viewer>
  <img src="../image/linebar-7.png" width="60%">
  </viewer>

- 显示最大最小值开关：控制每组折线图以及柱状图是否用气泡标记最大最小值，设置显示最大最小值，如下图；

  <viewer>
  <img src="../image/linebar-8.png" width="60%">
  </viewer>

### 8、动画

选中该折线图-柱状图组件，在操作界面右侧的“动画”处可修改组件的载入动画效果。分别为：弹跳、渐隐渐显、
向右滑入、向左滑入、放缩、旋转、滚动。

## 二、数据

静态数据格式：

```
    [
      [
        {
          "name":"蒸发量",
          "type":"bar",
          "data":[2,4.9,7,23.2,25.6,76.7,135.6,162.2,32.6,20,6.4,3.3]
        },
        {
          "name":"降水量",
          "type":"bar",
          "data":[2.6,5.9,9,26.4,28.7,70.7,175.6,182.2,48.7,18.8,6,2.3]
        },
        {
          "name":"平均温度",
          "type":"line",
          "data":[2,2.2,3.3,4.5,6.3,10.2,20.3,23.4,23,16.5,12,6.2]
        }
      ],
      ["1月","2月","3月","4月","5月","6月","7月","8月","9月","10月","11月","12月"]
    ]
```
