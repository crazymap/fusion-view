import request from '@/utils/request'

// 查询素材收藏列表
export function listFavorite(query) {
  return request({
    url: '/templates/favorite/list',
    method: 'get',
    params: query
  })
}

// 查询指定用户素材收藏列表
export function userFavoriteList(query) {
  return request({
    url: '/templates/favorite/user/list',
    method: 'get',
    params: query
  })
}

// 查询素材收藏详细
export function getFavorite(id) {
  return request({
    url: '/templates/favorite/' + id,
    method: 'get'
  })
}

// 新增素材收藏
export function addFavorite(data) {
  return request({
    url: '/templates/favorite',
    method: 'post',
    data: data
  })
}

// 修改素材收藏
export function updateFavorite(data) {
  return request({
    url: '/templates/favorite',
    method: 'put',
    data: data
  })
}

// 删除素材收藏
export function delFavorite(id) {
  return request({
    url: '/templates/favorite/' + id,
    method: 'delete'
  })
}

// 导出素材收藏
export function exportFavorite(query) {
  return request({
    url: '/templates/favorite/export',
    method: 'get',
    params: query
  })
}

// 验证素材收藏
export function isExisted(data) {
  return request({
    url: '/templates/favorite/isExisted',
    method: 'post',
    data: data
  })
}